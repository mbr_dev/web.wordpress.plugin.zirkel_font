<?php
/*
Plugin Name: Zirkel Font
Plugin URI: http://www.manuel-roesch.de
Description: Fügt einen Wildenstein Zirkel ein wenn man [Wld!] setzt.
Version: 1.0
Author: Manuel Rösch
Author URI: http://www.manuel-roesch.de
*/

function zirkel_font_enable_frontend_styles () {
    wp_register_style( 'zirkel_font_frontend_main_style', plugins_url('css/main.css', __FILE__ ) );
    wp_enqueue_style( 'zirkel_font_frontend_main_style' );
}
add_action( 'wp_enqueue_scripts', 'zirkel_font_enable_frontend_styles' );

$contains = array ('Wld', 'GW', 'Fre');

function zirkel_font_add_Wld_shortcode() {
    ob_start();
    echo '<span class="zirkel_icon-Wld"></span>';
    return ob_get_clean();
}
add_shortcode( 'Wld!', 'zirkel_font_add_Wld_shortcode' );

function zirkel_font_add_Fre_shortcode() {
    ob_start();
    echo '<span class="zirkel_icon-Fre"></span>';
    return ob_get_clean();
}
add_shortcode( 'Fre!', 'zirkel_font_add_Fre_shortcode' );

function zirkel_font_add_GW_shortcode() {
    ob_start();
    echo '<span class="zirkel_icon-GW"></span>';
    return ob_get_clean();
}
add_shortcode( 'GW!', 'zirkel_font_add_GW_shortcode' );

