Ein einfaches Plugin um Zirkel studentischer Korporationen in Wordpress nutzen zu können.

Bisher enthalten:

- [Fre!] K.D.St.V. Fredericia Bamberg
- [GW!] K.D.St.V. Gothia-Würzburg im CV
- [Wld!] K.D.St.V. Wildenstein zu Freiburg im Breisgau

Weitere Zirkel werden gerne implementiert hierzu bitte:

- Kürzel (CV served first)
- Zirkel (am liebsten .svg)
- Bundname

per Email an:
kroete@kdstv-wildenstein.de
